---
sidebar_position: 1
---

# Mining algorithms

The Text and Data Mining (TDM) algorithms used for enriching the OpenAIRE Graph are grouped in the following main categories:

[Extraction of acknowledged concepts](acks.md)

[Extraction of cited concepts](cites.md)

[Document Classification](classified.md)

<span className="todo">TODO</span>






